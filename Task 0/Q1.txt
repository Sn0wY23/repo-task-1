Git is the most widely used Version Control System (VCS) - a system that tracks proges made in a project and records snapshots of any changes made to said project, 
enabling the users to revert back to any given snapshot, fast and easy. 
It's a Distributed VCS, meaning that, unlike the Centralized VCSs (like 'Subversion' or 'Microsoft TFS'), 
each user has a copy of the infomation on their local machine, eliminating the single point of failiure, being the centralized server.

GIT allows for:

- tracking changes made to a project;
- taking snapshots of any given state of the project and enabling a backups option for each instance;
- a team of Developers to work on a project simultaniously;
- working on a project localy or, along with GitHub/Lab to integrate the reposetory for automating a build for the project, adding substantial flexibility to the system;
- CLI access;