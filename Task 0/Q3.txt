The CAP theorem states that any distributed data store (DDS), be it a database or network, can only guarantee 2 of the folloing:

- Consistency - every read recieves a write or an error;
- Availavility - every request recieves a valid responce, without the guarantee of containing the most recent write;
- Partition (tolerance) - the system continues operating, despite encountring an issue with, like droping or delaying messages;

In other words, a DDS can either, after a netowork partition event:

a) Stay consistent and provide stable opertions, but risk downtime;
b) Stay available, but possibly compromize consistent operations; 

This fact stems from the nature of the design of databases itsself and how it impacts destribution.

There has beed an extencion added to the CAP theorem in the last decade, called the PACELC theorem.

This theorem states that, when there isn't a network partition event, the DDS must still choose between
latency and consistency, thus crating a sort of middle option in the CAP theorem. That option being 
a compromise in consistency for a boost in latency, aiming for a balance between the two.