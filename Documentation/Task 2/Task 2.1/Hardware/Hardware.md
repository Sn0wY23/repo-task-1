*H/W path*     *Device*      *Class*          *Description*
___               
=================================================== 
___                
                              **system**       **vServer**
                 
/0                            bus            Motherboard
/0/0                          memory         96KiB BIOS
/0/400                        processor      AMD EPYC Processor
/0/1000                       memory         2000MiB System Memory
/0/1000/0                     memory         2000MiB DIMM RAM
/0/100                        bridge         82G33/G31/P35/P31 Express DRAM Cont
/0/100/1                      display        Virtio GPU
/0/100/1/0                    generic        Virtual I/O device
/0/100/2                      bridge         QEMU PCIe Root port
/0/100/2/0                    network        Virtio network device
/0/100/2/0/0      eth0        network        Ethernet interface
/0/100/2.1                    bridge         QEMU PCIe Root port
/0/100/2.1/0                  bus            QEMU XHCI Host Controller
/0/100/2.1/0/0    usb1        bus            xHCI Host Controller
/0/100/2.1/0/0/1              input          QEMU USB Tablet
/0/100/2.1/0/1    usb2        bus            xHCI Host Controller
/0/100/2.2                    bridge         QEMU PCIe Root port
/0/100/2.2/0                  communication  Virtio console
/0/100/2.2/0/0                generic        Virtual I/O device
/0/100/2.3                    bridge         QEMU PCIe Root port
/0/100/2.3/0                  generic        Virtio memory balloon
/0/100/2.3/0/0                generic        Virtual I/O device
/0/100/2.4                    bridge         QEMU PCIe Root port
/0/100/2.4/0                  generic        Virtio RNG
/0/100/2.4/0/0                generic        Virtual I/O device
/0/100/2.5                    bridge         QEMU PCIe Root port
/0/100/2.5/0                  storage        Virtio SCSI
/0/100/2.5/0/0                generic        Virtual I/O device
/0/100/2.6                    bridge         QEMU PCIe Root port
/0/100/2.7                    bridge         QEMU PCIe Root port
/0/100/3                      bridge         QEMU PCIe Root port
/0/100/1f                     bridge         82801IB (ICH9) LPC Interface Contro
/0/100/1f.2                   storage        82801IR/IO/IH (ICH9R/DO/DH) 6 port
/0/100/1f.3                   bus            82801I (ICH9 Family) SMBus Controll
/0/1                          system         PnP device PNP0b00
/0/2                          input          PnP device PNP0303
/0/3                          input          PnP device PNP0f13
/0/4                          communication  PnP device PNP0501
/0/5              scsi0       storage
/0/5/0.0.0        /dev/sda    disk           40GB QEMU HARDDISK
/0/5/0.0.0/1      /dev/sda1   volume         37GiB EXT4 volume
/0/5/0.0.0/e      /dev/sda14  volume         1023KiB BIOS Boot partition
/0/5/0.0.0/f      /dev/sda15  volume         255MiB Windows FAT volume
/0/6              scsi1       storage        
/0/6/0.0.0        /dev/cdrom  disk           QEMU DVD-ROM
___