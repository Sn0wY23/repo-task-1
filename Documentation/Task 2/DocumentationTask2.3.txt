1. Check ssh port (should be 22by default) - netstat -tulnp | grep ssh
     tcp        0      0 0.0.0.0:22

2. Change port number:
   - Edit sshd_config file - nano /etc/ssh/sshd_config;
   - Uncomment Portnumber;
   - Port set to 2022;
   - Save and exit sshd_config file;
   - Restart the ssh client - systemctl restart sshd;
   - Check configuration: 
     tcp        0      0 0.0.0.0:2022
   - Note new login data:
     ssh -p 2022 'user'@'IP'

3. Exit VPS and commit changes to working copy.